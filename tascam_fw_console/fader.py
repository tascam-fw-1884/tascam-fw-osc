#!/usr/bin/env python3
"""
    Open Sound Control send/recieve daemon for Tascam Firewire control surface
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    :copyright: Copyright (c) 2018 Scott Bahling
        This program is free software; you can redistribute it and/or modify
        it under the terms of the GNU General Public License version 2 as
        published by the Free Software Foundation.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program (see the file COPYING); if not, write to the
        Free Software Foundation, Inc.,
        51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
    :license: GPL-2.0, see COPYING for details
"""

from tascam_fw_console import osc

state_quadlets = (4, 0, 0, 1, 1, 2, 2, 3, 3)
state_bits = ((1, 16),
               (1, 16),
               (17, 32),
               (1, 16),
               (17, 32),
               (1, 16),
               (17, 32),
               (1, 16),
               (17, 32),
               )


class Fader():
    def __init__(self, strip):
        self.name = 'Strip {} Fader'. format(strip.num)
        self.strip = strip
        self.console = strip.console
        if self.strip.num == 0:
            self.addr = '/master/fader'
            self.press_args = (self.strip.num, 1)
            self.release_args = (self.strip.num, 0)
        else:
            self.addr = '/strip/fader'
            self.press_args = (1,)
            self.release_args = (0,)
        self.state_quadlet = state_quadlets[self.strip.num]
        self.state_bits = state_bits[self.strip.num]
        self.state_callback = self.send_pos

    @property
    def position(self):
        return self.console.state.field(self.state_quadlet, *self.state_bits)

    @position.setter
    def position(self, pos):
        self.console.unit.strips[self.strip.num].fader.set_position(pos)

    def send_pos(self, pos):
        if self.strip.num == 0:
            osc.client.send_message(self.addr, pos)
        else:
            osc.client.send_message(self.addr, (self.strip.num, pos))

    def press(self):
        addr = '{}_touch'.format(self.addr)
        args = self.press_args + (1,)
        osc.client.send_message(addr, args)
        self.console.state_thread.add_callback(self)

    def release(self):
        addr = '{}_touch'.format(self.addr)
        args = self.press_args + (0,)
        osc.client.send_message(addr, args)
        self.console.state_thread.remove_callback(self)
