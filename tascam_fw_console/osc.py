#!/usr/bin/env python3
"""
    Open Sound Control send/recieve daemon for Tascam Firewire control surface
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    :copyright: Copyright (c) 2018 Scott Bahling
        This program is free software; you can redistribute it and/or modify
        it under the terms of the GNU General Public License version 2 as
        published by the Free Software Foundation.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program (see the file COPYING); if not, write to the
        Free Software Foundation, Inc.,
        51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
    :license: GPL-2.0, see COPYING for details
"""

from pythonosc import dispatcher
from pythonosc import osc_server
from pythonosc import udp_client

client = None
server = None
dispatcher = dispatcher.Dispatcher()


class OSCClient(udp_client.SimpleUDPClient):
    def __init__(self, ip, port):
        super().__init__(ip, port)

    def send_message(self, addr, args):
        try:
            print(addr, *args)
        except Exception:
            print(addr, args)

        super().send_message(addr, args)


def init_dispatcher(console):
    dispatcher.map("/loop_toggle", console.loop_toggle_handler)
    dispatcher.map("/transport_stop", console.transport_stop_handler)
    dispatcher.map("/transport_play", console.transport_play_handler)
    dispatcher.map("/ffwd", console.ffwd_handler)
    dispatcher.map("/rewind", console.rewind_handler)
    dispatcher.map("/rec_enable_toggle", console.rec_enable_toggle_handler)
    dispatcher.map('/strip/fader', console.strip_fader_handler)
    dispatcher.map('/strip/mute', console.strip_mute_handler)
    dispatcher.map('/strip/select', console.strip_select_handler)
    dispatcher.map('/strip/solo', console.strip_solo_handler)
    dispatcher.map('/strip/recenable', console.strip_recenable_handler)
    dispatcher.map('/master/fader', console.master_fader_handler)
    dispatcher.map('/bank_up', console.bank_up_handler)
    dispatcher.map('/bank_down', console.bank_down_handler)
    dispatcher.map('/encoder_mode', console.encoder_mode_handler)
    dispatcher.set_default_handler(console.default_handler)


def init_client(ip, port):
    global client
    client = OSCClient(ip, port)


def init_server(ip, port, console):
    global server
    init_dispatcher(console)
    server = osc_server.ThreadingOSCUDPServer((ip, port), dispatcher)
