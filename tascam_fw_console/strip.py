#!/usr/bin/env python3
"""
    Open Sound Control send/recieve daemon for Tascam Firewire control surface
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    :copyright: Copyright (c) 2018 Scott Bahling
        This program is free software; you can redistribute it and/or modify
        it under the terms of the GNU General Public License version 2 as
        published by the Free Software Foundation.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program (see the file COPYING); if not, write to the
        Free Software Foundation, Inc.,
        51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
    :license: GPL-2.0, see COPYING for details
"""

from tascam_fw_console.fader import Fader
from tascam_fw_console.encoder import Encoder


class Strip():

    def __init__(self, console, num):
        self.console = console
        self.num = num
        self.mute_button = None
        self._mute = False
        self.solo_button = None
        self._solo = False
        self.select_button = None
        self._select = False
        self._rec = False
        self.encoder = Encoder('Strip {}'.format(self.num),
                               '/strip/encoder',
                               self.num,
                               )
        self.fader = Fader(self)

    @property
    def mute_led(self):
        return self.console.unit.strips[self.num].mute_led

    @property
    def solo_led(self):
        return self.console.unit.strips[self.num].solo_led

    @property
    def sel_led(self):
        return self.console.unit.strips[self.num].sel_led

    @property
    def rec_led(self):
        return self.console.unit.strips[self.num].rec_led

    @property
    def mute(self):
        return self._mute

    @mute.setter
    def mute(self, state):
        state = bool(state)
        self._mute = state
        if state:
            self.mute_led.turn_on()
        else:
            self.mute_led.turn_off()

    @property
    def solo(self):
        return self._solo

    @solo.setter
    def solo(self, state):
        state = bool(state)
        self._solo = state
        if state:
            self.solo_led.turn_on()
        else:
            self.solo_led.turn_off()

    @property
    def select(self):
        return self._select

    @select.setter
    def select(self, state):
        state = bool(state)
        self._select = state
        if state:
            self.sel_led.turn_on()
        else:
            self.sel_led.turn_off()

    @property
    def rec(self):
        return self._rec

    @rec.setter
    def rec(self, state):
        state = bool(state)
        self._rec = state
        if state:
            self.rec_led.turn_on()
        else:
            self.rec_led.turn_off()
