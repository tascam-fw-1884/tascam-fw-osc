#!/usr/bin/env python3
"""
    Open Sound Control send/recieve daemon for Tascam Firewire control surface
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    :copyright: Copyright (c) 2018 Scott Bahling
        This program is free software; you can redistribute it and/or modify
        it under the terms of the GNU General Public License version 2 as
        published by the Free Software Foundation.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program (see the file COPYING); if not, write to the
        Free Software Foundation, Inc.,
        51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
    :license: GPL-2.0, see COPYING for details

Usage:
    tascam-fw-osc [options]

Options:
    -h --help                    Show this screen.
    -l --list                    List connected Tascam FW console units
    -c CARD_NUM --card=CARD_NUM  Number of the ALSA sound card
    -g GUID --guid=GUID          GUID of the FW unit
    -d DEVICE --device=DEVICE    Path to device under /dev
    -I IP --listen-ip=IP         IP to listen on [default: 127.0.0.1]
    -P PORT --listen-port=PORT   Port to listen on [default: 8100]
    -i IP --ip=IP                IP to communicate to [default: 127.0.0.1]
    -p PORT --port=PORT          Port to communicate to [default: 9100]
"""

from docopt import docopt
from tascam_fw_console.console import Console, list_units
from tascam_fw_console import osc


def main():

    args = docopt(__doc__, version='0.1')

    if args['--list']:
        for model, fullpath, guid in list_units():
            print('{0} path: {1} GUID: 0x{2:016x}'.format(model,
                                                          fullpath, guid))
        exit()

    console = Console(card_id=args['--card'],
                      guid=args['--guid'],
                      device=args['--device'])
    osc.init_client(args['--ip'], int(args['--port']))
    osc.init_server(args['--listen-ip'], int(args['--listen-port']), console)

    print("Sending to {}:{}".format(osc.client._address, osc.client._port))
    print("Serving on {}".format(osc.server.server_address))
    osc.server.serve_forever()


if __name__ == '__main__':
    main()
