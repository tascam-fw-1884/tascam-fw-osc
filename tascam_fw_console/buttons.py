#!/usr/bin/env python3
"""
    Open Sound Control send/recieve daemon for Tascam Firewire control surface
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    :copyright: Copyright (c) 2018 Scott Bahling
        This program is free software; you can redistribute it and/or modify
        it under the terms of the GNU General Public License version 2 as
        published by the Free Software Foundation.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program (see the file COPYING); if not, write to the
        Free Software Foundation, Inc.,
        51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
    :license: GPL-2.0, see COPYING for details
"""

import re
from tascam_fw_console import osc


keymap = {'CTRL': 'ctrl',
          'ALT': 'alt',
          'SHIFT': 'shift',
          }


osc_addrs = {'STOP': '/transport_stop',
             'PLAY': '/transport_play',
             'F.FWD': '/ffwd',
             'REW': '/rewind',
             'REC': '/rec_enable_toggle',
             }

re_strip_num = re.compile('.*([1-9])')


class Button():
    def __init__(self, console, name, addr):
        self.name = name
        self.console = console
        self.addr = '/button/{}'.format(addr)
        osc.dispatcher.map(self.addr, self.osc_handle)

    def press(self):
        osc.client.send_message(self.addr, 1)

    def release(self):
        osc.client.send_message(self.addr, 0)

    def osc_handle(self, addr, enable):
        print(self.name)
        if enable:
            self.console.unit.leds.turn_on(self.name)
        else:
            self.console.unit.leds.turn_off(self.name)


class StripButton(Button):
    def __init__(self, console, name, addr, strip):
        super().__init__(console, name, addr)
        self.strip = console.strips[strip]
        button = addr.split('/')[-1]
        if button == 'sel':
            self.strip.select_button = self
            self.osc_handler = console.strip_select_handler
        elif button == 'mute':
            self.strip.mute_button = self
            self.osc_handler = console.strip_mute_handler
        elif button == 'solo':
            self.strip.solo_button = self
            self.osc_handler = console.strip_solo_handler

    def press(self):
        osc.client.send_message(self.addr, (self.strip.num, 1))

    def release(self):
        osc.client.send_message(self.addr, (self.strip.num, 0))
