#!/usr/bin/env python3
from setuptools import setup
import versioneer

PROJECT = 'tascam_fw_console'

setup(name=PROJECT,
      version=versioneer.get_version(),
      cmdclass=versioneer.get_cmdclass(),
      author='Scott Bahling',
      author_email='sbahling@mudgum.net',
      packages=[PROJECT],
      entry_points = {
          'console_scripts': ['tascam-fw-console=tascam_fw_console.cli:main'],
          },
      )
