
Open Sound Control for Tascam Firewire control surfaces
=======================================================

This is a simple daemon that reacts on received OSC messages to control the
Tascam surface (update LEDs and Faders) as well as sends OSC messages
based on user input via the control surface (pressing buttons, moving
faders and dials).

Currently only the Tascam FW-1884 model is enabled and tested.

Requirements
------------

* Recent kernel with Tascam FW status control support  
  (4.20 or later. 5.3 or later recommended).
* libhinawa 2.0.0 or later.  
  https://github.com/takaswie/libhinawa/
* Python tascam_fw module  
  https://gitlab.com/tascam-fw-1884/tascam-fw

